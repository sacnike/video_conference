let express = require('express');
let app = express();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let stream = require('./ws/stream');
let db = require('./ws/queries');
let path = require('path');
let favicon = require('serve-favicon')
let LogRocket = require('logrocket');


LogRocket.init('zz6wer/heroku');


app.use(favicon(path.join(__dirname, 'favicon.ico')));
app.use('/assets', express.static(path.join(__dirname, 'assets')));

app.get('/', (req, res)=>{
    res.sendFile(__dirname+'/index.html');
});

io.of('/stream').on('connection', stream);

server.listen(process.env.PORT || 3000);

//db.saveUser('Sachin','Patil','9892345678','someone@example.com','12345678');